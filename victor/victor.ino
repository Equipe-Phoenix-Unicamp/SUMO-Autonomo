#import "TimerOne.h"
void setup()
{
  Serial.begin(9600);
  Timer1.initialize(10);  
  configMotor(10);
  configMotor(9);
}

int  i = 100;
void loop()
{

  if (i > -100)
    OnFwd(9, i);
  i--;
  delay(500);
  Serial.print(i);
  Serial.print("\t");
}
void configMotor(uint8_t motorPin)
{
  pinMode(motorPin,OUTPUT);
  Timer1.pwm(motorPin, 92, 17000);
  //Zero do victor com codigo de barras eh 94
  //Zero do outro victor eh  no 91
}
void OnRev(uint8_t motorPin, int pwm)
{
  OnFwd(motorPin, -pwm);
}
//Barras e sem barras eh o que diferencia os victors. Como a calibragem eh diferente
//as funcoes sao difetentes
void OnFwd_SemBarras(uint8_t motorPin, int8_t pwm)//Zero em 91
{
  int dutyCycle = ((((pwm + 100)/200.0) +1)/17.0 )*1024; //Duty Cycle 
  Serial.println(dutyCycle);
  Timer1.setPwmDuty(motorPin, dutyCycle);
}
void OnFwd_Barras(uint8_t motorPin, int8_t pwm)//Zero em 94
{
  int dutyCycle = ((((pwm + 100)/200.0) +1)/17.0 )*1024; //Duty Cycle 
  Serial.println(dutyCycle);
  Timer1.setPwmDuty(motorPin, dutyCycle);
}

