//Obs: Sensores de linha da frente retornao 0 no branco. Foi utilizado um 7408 (AND)
//Para conectar dois sensores na mesma porta, pois arduino UNO so tem 2 pinos de interrupt
//externo

volatile int linha = 0; //0 = Preto | 1 = Linha encontrada na frente | -1 = Linha encontrada atras
void setup() {
  Serial.begin(9600);
  pinMode(2,INPUT);
  pinMode(3,INPUT);
  attachInterrupt(0, frente,FALLING);
  attachInterrupt(1, tras,FALLING);
}

void loop() {
  
}
void frente()
{
  linha = 1;
  //LINHA FRENTE
  return;
}
void tras()
{
  linha = -1;
  //LINHA ATRAS
  return;
}
